# This file is executed on every boot (including wake-boot from deepsleep)
import time
import machine
from settings import WIFI_SSID, WIFI_PASSWORD, MQTT_BROKER_IP, MQTT_BROKER_PORT
import ubinascii
from umqtt.simple import MQTTClient

# get id
DEVICE_ID = b"esp8266_" + ubinascii.hexlify(machine.unique_id())

def connect():
    # connect to wifi
    import network
    wlan = network.WLAN(network.STA_IF)
    wlan.active(True)
    if not wlan.isconnected():
        print('connecting to network...')
        wlan.connect(WIFI_SSID, WIFI_PASSWORD)
        while not wlan.isconnected():
            pass

def main():
    # connect to mqtt server
    client = MQTTClient(DEVICE_ID, MQTT_BROKER_IP, MQTT_BROKER_PORT)
    client.connect()


    analog_sensor = machine.ADC(0)
    while True:
        value = analog_sensor.read()
        client.publish(b"feeds/light/{}".format(DEVICE_ID), bytes(str(value), 'utf-8'))
        print('Sensor state: {}'.format(value))
        time.sleep(5)

if __name__ == '__main__':
    connect()
    main()
