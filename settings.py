WIFI_SSID = "yourwifinetwork"
WIFI_PASSWORD = "wifipassword"
MQTT_BROKER_IP = "localhost"
MQTT_BROKER_PORT = 1883

try:
    from local_settings import *
except ImportError:
    pass
